<?php

require_once 'Appconfig.php';
require_once 'Dumpit.php';

try {
  //show stats tables
  show_generated_total_weight_table();
  possible_combinations(); 
} 
catch ( Exception $e ) {
  dump_it( $e->getTraceAsString() );
}