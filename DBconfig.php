<?php

class DBconfig {
  public static function get_instance(){
    if (is_null(self::$db)){
      self::$db = new DBconfig();
      self::$db->config_filling();
      self::$db->mysqli();
      return self::$db;
    }
    return self::$db;
  }
  private function __construct(){}
  public function __destruct() {
    $this->close();
  }
  private function __clone(){} 
  private function __wakeup(){} 
  protected static $db;
  public $mysqli_link;
  public $username, $hostname, $pass, $dbname;

  function config_filling(){
    //production
    if ($_SERVER['HTTP_HOST'] == 'bitbandit.eu'){
      $this->dbname = 'bitbandi_game';
      $this->username = 'bitbandi_game';
      $this->hostname = '';
      $this->pass = 'h-+LL3$_]oTX';
      $this->port = 3306;
      $_SESSION['host'] = 'production';
    }
    else{//if ($_SERVER['HTTP_HOST'] == '109.174.40.94' || $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '127.0.0.1'){
      $this->dbname = 'slot_db';
      $this->username = 'slot_user';
      $this->hostname = '127.0.0.1';
      $this->pass = 'slot_pass';
      $this->port = 3306;
      $_SESSION['host'] = 'localhost';
    }
  }

  public function mysqli(){
    $this->mysqli_link = new mysqli( $this->hostname, $this->username, $this->pass, $this->dbname, $this->port );
    try{
      if ( $this->mysqli_link->connect_error ) {
        die( 'Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
      }
    }
    catch ( Exception $e ) {
      $error_message = "Failed to make query to DB. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
  }
  
  public function query( $query_string ){
    try {
      $result = $this->mysqli_link->query( $query_string );
      if ( !$result ){
        $error_message = "Failed to make query to DB. File ".__FILE__." Line " .__LINE__;
        $error_message .= $this->mysqli_link->error.' <br> '.$query_string;
        error_log( $error_message, 0 );
      }
    }
    catch ( Exception $e ) {
      $error_message = "Failed to make query to DB. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
    return $result;
  }

  public function mysqli_fetch_array( $query_string ){
    try{
      $q = $this->query( $query_string );
      $result = $q->fetch_array();
    }
    catch ( Exception $e ){
      $error_message = "Failed to make query to DB. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
    return $result;
  }

  public function mysqli_fetch_array_by_result( $res ){
    try {
       $row = $res->fetch_array();
    }
    catch ( Exception $e ) {
      $error_message = "Failed to make query to DB. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
    return $row;
  }

  public function close(){
    return $this->mysqli_link->close();
  }
}