<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vadim28416
 * Date: 15.06.13
 * Time: 19:38
 * To change this template use File | Settings | File Templates.
 */


class AffiliateLinkTest extends \PHPUnit_Framework_TestCase {
  function test_construct(){
    $al1 = new AffiliateLink();
    $this->assertTrue( $al1->affiliateusername === '', $al1->referred_uid === '', $al1->total_earned === 0, $al1->payed === 0 );

    $al2 = new AffiliateLink('myUserA', '29d5a28294033e7d558b24dc09bbde92e5cdde37', 34.45, 54.01);
    $this->assertTrue( $al2->affiliateusername === 'myUserA', $al2->referred_uid === '29d5a28294033e7d558b24dc09bbde92e5cdde37', $al2->total_earned === 34.45, $al2->payed === 54.01 );

  }
  function test_real_escape(){
    $al = new AffiliateLink('my\'Use\'rA', '29d5\'a28294033e7d558b2\'4dc09bbde92e5cdde37', 34.45, 54.01);
    $this->assertTrue( $al->affiliateusername === 'my\'Use\'rA', $al->referred_uid === '29d5\'a28294033e7d558b2\'4dc09bbde92e5cdde37', $al->total_earned === 34.45, $al->payed === 54.01 );
  }
  function test_is_link_exist(){
    //existed_link
    $existed_affiliateusername = 'userA';
    $existed_referred_uid= 'a571fa836b011800e09e50281af6ceaaa1f8f82e';
    $this->assertTrue( AffiliateLink::is_link_exist_for( $existed_affiliateusername, $existed_referred_uid ) );
    //not_existed_link
    $not_existed_affiliateusername = 'userAasdf';
    $not_existed_referred_uid= 'a571fa8aaa800e09e50281af6ceaaa1f8aaae';
    $this->assertFalse( AffiliateLink::is_link_exist_for( $not_existed_affiliateusername, $not_existed_referred_uid ) );

    $new_affiliateusername = 'userUser';
    $new_referred_uid= 'a571fa8111111800e09e50281af6ceaaa1f8f82e';
    $new_al = new AffiliateLink( $new_affiliateusername, $new_referred_uid );
    $new_al->save_affiliate_link();
    $this->assertTrue( $new_al->is_link_exist( $new_affiliateusername, $new_referred_uid ) );
  }

  function test_is_valid_all_are_upper(){
    $valid_affiliateusername = '';
    $valid_referred_uid = '1e52e0a1a74c04cf560bc536efe342cac69507e5';
    $valid_total_earned = 123.123;
    $valid_payed = 234.234;

    for ( $characters_counter = 0; $characters_counter < 70; $characters_counter++ ){
      //characters in $valid_affiliateusername
      for ( $case = 0; $case < 3; $case++ ){
        $valid_affiliateusername = '';
        $valid_al = new AffiliateLink( $valid_affiliateusername, $valid_referred_uid, $valid_total_earned, $valid_payed );
        if ( $characters_counter >= 3 && $characters_counter <= 64 ){
          $this->assertTrue( $valid_al->is_valid() );
        }
        else{
          $this->assertFalse( $valid_al->is_valid() );
        }
        unset( $valid_al );
        //upper case
        if ( $case == 0 ){
          $valid_affiliateusername .= chr( 65 + $characters_counter % 26  );//A-Z
        }
        //low
        elseif ( $case == 1 ){
          $valid_affiliateusername .= chr( 97 + $characters_counter % 26  );//a-z
        }
        //number
        elseif ( $case == 2 ){
          $valid_affiliateusername .= chr( $characters_counter % 10  );//0-9
        }
      }
    }
  }


  function test_is_valid_all_good(){
    $valid_affiliateusername = ''; // <= 64 chars
    $valid_referred_uid = '1e52e0a1a74c04cf560bc536efe342cac69507e5';
    $valid_total_earned = 123.123;
    $valid_payed = 234.234;

    for ( $i = 0; $i < 80; $i++ ){
      $valid_al = new AffiliateLink( $valid_affiliateusername, $valid_referred_uid, $valid_total_earned, $valid_payed );
      if ( $i < 3 || $i > 64 ){
        $this->assertFalse( $valid_al->is_valid() );
      }
      if ( $i >= 3 && $i <= 64 ){
        $this->assertTrue( $valid_al->is_valid() );
      }
      unset( $valid_al );
      $valid_affiliateusername .= chr( 65 + $i % 26 );
    }
  }

  function test_is_valid(){
    $valid_affiliateusername1 = 'userABCDEFGHijklmnopqrstuvwxyz123456789'; //39 chars
    $valid_referred_uid1 = '1e52e0a1a74c04cf560bc536efe342cac69507e5';
    $valid_total_earned1 = 123.123;
    $valid_payed1= 234.234;
    $valid_total_earned2 = '123.123';
    $valid_payed2= '234.234';

    $invalid_affiliateusername1 = 'userABCDEFG*/-@#$%^&*()Hijklmno456789';
    $invalid_referred_uid1 = '1e52e0*/48#$%^&*((S)536efe342cac69507e5';
    $invalid_total_earned1 = '%&$Ffd684*';
    $invalid_payed1 = 'abc^%^&SDF';

    $valid_al1 = new AffiliateLink( $valid_affiliateusername1, $valid_referred_uid1, $valid_total_earned1, $valid_payed1 );
    $this->assertTrue( $valid_al1->is_valid() );

    $valid_al2 = new AffiliateLink( $valid_affiliateusername1, $valid_referred_uid1, $valid_total_earned2, $valid_payed2 );
    $this->assertTrue( $valid_al2->is_valid() );

    $invalid_al1 = new AffiliateLink( );
    $invalid_al2 = new AffiliateLink( $valid_affiliateusername1, $valid_referred_uid1, $valid_total_earned1, $invalid_payed1 );
    $this->assertFalse( $invalid_al2->is_valid() );

    $invalid_al3 = new AffiliateLink( $invalid_affiliateusername1, $valid_referred_uid1, $valid_total_earned1, $valid_payed1 );
    $this->assertFalse( $invalid_al3->is_valid() );

    $invalid_al4 = new AffiliateLink( $valid_affiliateusername1, $invalid_referred_uid1, $valid_total_earned1, $valid_payed1 );
    $this->assertFalse( $invalid_al4->is_valid() );

    $invalid_al5 = new AffiliateLink( $valid_affiliateusername1, $valid_referred_uid1, $invalid_total_earned1, $valid_payed1 );
    $this->assertFalse( $invalid_al5->is_valid() );

    $invalid_al6 = new AffiliateLink( $valid_affiliateusername1, $valid_referred_uid1, $valid_total_earned1, $invalid_payed1 );
    $this->assertFalse( $invalid_al6->is_valid() );

  }

}
