<?php

function logout(){
    //clear session and cookie
    session_unset();
    session_destroy();
    session_id(null);
    //clear SID in COOKIES
}

function show_interesting_facts() {
  $user = $_SESSION['User'];
  $slot = $_SESSION['Slot'];
  $total_cached_out = Transaction::get_total_cashed_out_money();
  $total_spin_number = $slot->get_total_spin_number();
  $output = "
    <div id=\"interesting_facts\">
      <table class=\"table\" border='2px'>
        <tr>
          <td id='total_cached_out'>Cashed out money: $total_cached_out BTC</td>
          <td id='total_spin_number'>Games played: $total_spin_number </td>
        </tr>
      </table>
    </div>
  ";
  echo $output;
}

function show_generated_total_weight_table() {
  $user = $_SESSION['User'];
  $slot = $_SESSION['Slot'];
  $paytable = Paytable::get_instance();
  echo '<table border="1px" style="border-collapse: collapse;">';
  echo '<tr>';
    echo '<td>';
    echo '---';
    echo '</td>';
    echo '<td>';
    echo Symbol::$pyramid;
    echo '</td>';
    echo '<td>';
    echo Symbol::$bitcoin;
    echo '</td>';
    echo '<td>';
    echo Symbol::$anonymous;
    echo '</td>';
    echo '<td>';
    echo Symbol::$onion;
    echo '</td>';
    echo '<td>';
    echo Symbol::$anarchy;
    echo '</td>';
    echo '<td>';
    echo Symbol::$peace;
    echo '</td>';
    echo '<td>';
    echo Symbol::$blank;
    echo '</td>';
    echo '<td>';
    echo 'Sum';
    echo '</td>';
    echo '</tr>';
  $number_of_symbol = array();
  for ($i = 0; $i < 3; $i++){
    $number_of_symbol[$i][Symbol::$pyramid] = 0;
    $number_of_symbol[$i][Symbol::$bitcoin] = 0;
    $number_of_symbol[$i][Symbol::$anonymous] = 0;
    $number_of_symbol[$i][Symbol::$onion] = 0;
    $number_of_symbol[$i][Symbol::$anarchy] = 0;
    $number_of_symbol[$i][Symbol::$peace] = 0;
    $number_of_symbol[$i][Symbol::$blank] = 0;
  }
  for($reel_num = 0; $reel_num < 3; $reel_num++){
    echo '<tr>';
    for ($i = 0; $i < 64; $i++){
      //emulate spin and generating new server seeds
      $slot->generateServerSeeds();
      $server_seeds = $slot->getServerSeeds();
      //emulate client seed getting
      $client_seed = mt_rand(0, 2000000);
      $hashed_client_seed = sha1($client_seed);
      $cur_sym = $slot->reels[$reel_num]->get_new_randomly_choosed_symbol($hashed_client_seed, $server_seeds[$reel_num]);

      foreach ($number_of_symbol[$reel_num] as $key => $value) {
        //e.g.: if (Symbol::$pyramid == $cur_sym)
        if ($key == $cur_sym){
          //e.g.: $number_of_symbol[$reel_num][Symbol::$bitcoin]++;
          $number_of_symbol[$reel_num][$key]++;
        }
      }
    }
    echo '<td>';
    echo 'reel #'.$reel_num;
    echo '</td>';
    $sum = 0;
    foreach ($number_of_symbol[$reel_num] as $key => $value) {
      //count total weight
      $sum += $value;
      echo '<td>'.$value;
      echo '</td>';
    }
    echo '<td>';
    echo $sum;
    echo '</td>';
    echo '</tr>';
  }
  echo '</table>';
}

function possible_combinations(){
  $user = $_SESSION['User'];
  $slot = $_SESSION['Slot'];
  $paytable = Paytable::get_instance();
  
  $number_of_win_lines[Symbol::$pyramid] = 0;
  $number_of_win_lines[Symbol::$bitcoin] = 0;
  $number_of_win_lines[Symbol::$anonymous] = 0;
  $number_of_win_lines[Symbol::$onion] = 0;
  $number_of_win_lines[Symbol::$anarchy] = 0;
  $number_of_win_lines[Symbol::$peace] = 0;
  $number_of_win_lines[Symbol::$blank] = 0;
  $number_of_win_lines['bitcoin_2'] = 0;
  $number_of_win_lines['bitcoin_1'] = 0;
  $number_of_win_lines['lose'] = 0;
  //$N = 262144;
  $N = 2000;
  echo '<br /><table border="1px" style="border-collapse: collapse;">';
  for($i = 0; $i < $N; $i++){
    $client_seed = mt_rand(0, 2000000);
    $new_payline_and_servers_seeds = $slot->get_new_payline_and_servers_seeds($client_seed);
    $new_payline = $new_payline_and_servers_seeds['new_payline'];
    $server_seeds = $new_payline_and_servers_seeds['server_seeds'];
    $result = $paytable->paylines_matching_with_wins($new_payline);
    switch ($result){
      case 'pyramid_3': 
        $number_of_win_lines[Symbol::$pyramid]++;
        break;
      case 'bitcoin_3': 
        $number_of_win_lines[Symbol::$bitcoin]++;
        break;
      case 'anonymous_3': 
        $number_of_win_lines[Symbol::$anonymous]++;
        break;
      case 'onion_3': 
        $number_of_win_lines[Symbol::$onion]++;
        break;
      case 'anarchy_3': 
        $number_of_win_lines[Symbol::$anarchy]++;
        break;
      case 'peace_3': 
        $number_of_win_lines[Symbol::$peace]++;
        break;
      case 'blank_3': 
        $number_of_win_lines[Symbol::$blank]++;
        break;
      case 'bitcoin_2': 
        $number_of_win_lines['bitcoin_2']++;
        break;
      case 'bitcoin_1': 
        $number_of_win_lines['bitcoin_1']++;
        break;
      case 'lose': 
        $number_of_win_lines['lose']++;
        break;
    }
  }
  echo '</table>';
  //--- Table ---
  echo 'Table of amount of wins for '.$N.' stops. <br /> Most combinations (excepts bitcoin_2 - any 2 are bitcoins, bitcoin_1 - any 1 is bitcoin and lose) for 3 symbols )';
  echo '<br /><table border="1px" style="border-collapse: collapse;">';
  echo '<tr>';
  echo '<td>';
  echo 'win combination';
  echo '</td>';
  echo '<td>';
  echo 'combination occurrence';
  echo '</td>';
  echo '<td>';
  echo 'probability of win combination appears';
  echo '</td>';
  echo '<td>';
  echo '% money returning to player ( probability * payoff)';
  echo '</td>';
  echo '<td>';
  echo 'money (occurrence * bet * payoff - occurrence * bet)';
  echo '</td>';
  echo '</tr>';
  $client_bet = 1;
  $total_sum = 0;
  $total_probability_of_apear = 0;
  $total_money = 0;
  $probability_of_occur = 0;
  foreach ( $number_of_win_lines as $combination_name => $occurrence ) {
    echo '<tr>';
    echo '<td>'.$combination_name.'</td>';
    echo '<td>'.$occurrence.'</td>';
    $probability_of_occur = $occurrence/$N;
    echo '<td>'.$occurrence.' / ' .$N. ' = ' .$probability_of_occur.'</td>';
    $paytable = Paytable::get_instance();
    //get payoff for given combination name (key_...)
    if ( $combination_name == 'bitcoin_2' || $combination_name == 'bitcoin_1' ){
      $total_probability_of_apear += $probability_of_occur;
      $payoff = $paytable->payoff_value_by_name( $combination_name );
    }
    elseif ( $combination_name == 'lose' ){
      $payoff = 0;
    }
    elseif ( $combination_name == 'blank' ){
      $payoff = 0;
    }
    else{
      $total_probability_of_apear += $probability_of_occur;
      $payoff = $paytable->payoff_value_by_name( $combination_name.'_3' );
    }
    $res = $probability_of_occur * $payoff;
    if ( $res > 0 )
      $total_sum += $res;
    echo '<td>'. $probability_of_occur.' * '.$payoff. ' = ' .$res.'</td>';
    $money = $occurrence* $client_bet * $payoff - $occurrence * $client_bet;
    $total_money += $money;
    echo '<td>'.$occurrence. ' * ' .$client_bet. ' * ' .$payoff. ' - ' .$occurrence. ' * ' .$client_bet. '  = ' .$money.'</td>';
    echo '</tr>';
  }
  echo '<tr>';
  echo '<td>TOTAL:</td>';
  echo '<td>'.$N.'</td>';
  echo '<td>'.$total_probability_of_apear.  '</td>';
  echo '<td>'.$total_sum. ' ~ '. round( $total_sum*100, 2 ) .'% </td>';
  echo '<td> profit(deposited - paid) = '.$total_money*( -1 ).'</td>';
  echo '</tr>';
  echo '</table>';
  //--- Table ---
}