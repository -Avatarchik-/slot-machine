<?php
/**
 * Description of Slot
 *
 * @author vadim24816
 */

require_once 'Appconfig.php';

class Slot {
  protected static $slot;
  protected $log_every_spin;
  public $user;
  public static $bitcoin_account_name = 'SlotBank';
  public static $bitcoin_address;
  private function __construct(){}
  public function __destruct(){}
  private function __clone(){} 
  private function init($user){
    $this->user = $user;
  }
  public static function get_instance($user = null){
    if ( is_null(self::$slot) ){
      self::$slot = new Slot();
      //keep Slot in SESSION
      //init object
      self::$slot->init($user);
      //common account for all money in slot
      self::$bitcoin_account_name = 'SlotBank';
      $bitcoin_client_instance = MyBitcoinClient::get_instance();
      if ($bitcoin_client_instance->check_last_connect() === true){
        try{
          self::$bitcoin_address = $bitcoin_client_instance->getaccountaddress( self::$bitcoin_account_name );
        }
        catch ( Exception $e ){
          dump_it( $e->getTraceAsString() );
        }
      }
      self::$slot->log_every_spin = true;
      //copy weight table to slot
      $weight_table = WeightTable::get_instance();
      self::$slot->weight_table = $weight_table;
      //copy reel's weights from weight table to slot
      for ( $reelN = 0; $reelN < 3; $reelN++ ){
        self::$slot->reels[$reelN] = new Reel('reel'.$reelN);
        self::$slot->reels[$reelN]->reel_line = $weight_table->get_symbols_reel_line( $weight_table->symbol_weight_reels[$reelN] );
      }
      self::$slot->playing = 'on';
      self::$slot->paying_out = 'on';
      self::$slot->minbet = 0.001;
      self::$slot->maxbet = 1;
      self::$slot->percent_pay_to_referrers = 0;
      self::$slot->currentBet = 0;
      self::$slot->lastBet = 0;
      return self::$slot;
    }
    return self::$slot;
  }
  
  public
    $last_payline,
    $reels,
    $currentBet, $lastBet,
    $playing,
    $paying_out,
    $minbet,
    $maxbet,
    $percent_pay_to_referrers,
    $server_seeds;
  protected $weight_table;

  public function is_valid_client_seed($client_seed){
    if (!is_string($client_seed)){
      $error_message = "[Warning] Client seed is not string. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return false;
    }
    else{
      return true;
    }
  }

  /**
   * validate client's bet
   */
  public function is_valid_bet( $bet_from_client ){
    //not a number
    if ( !is_numeric( $bet_from_client )){
      $error_message = "[Warning] Client bet is not number. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return false;
    }
    //for free play spins
    if ( $bet_from_client == 0 ){
      return true;
    }
    //for real play spins
    if (  $bet_from_client > 0 && //should be greater than 0.001
          $bet_from_client >= $this->minbet && // 0 >= 0.001 is false, why minbet is needed?
          $bet_from_client <= $this->maxbet &&
          $bet_from_client <= $this->user->money_balance
          ){
      return true;
    }
    else {
      $error_message = "[Warning] Bet is not valid. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return false;
    }
  }

  public function get_total_spin_number(){
    $db = DBconfig::get_instance();
    $query = 'SELECT COUNT(id) FROM spins';
    $total_spin_number = $db->mysqli_fetch_array($query);
    return $total_spin_number[0];
  }

  public function save_spin_in_db( $uid, $user_bet, $payline, $won_money ){
    $db = DBconfig::get_instance();
    $uid =  $db->mysqli_link->real_escape_string($uid);
    $user_bet =  $db->mysqli_link->real_escape_string($user_bet);
    $payline =  $db->mysqli_link->real_escape_string($payline);
    $won_money =  $db->mysqli_link->real_escape_string($won_money);
    $res = $db->query("
      INSERT INTO
      spins(uid, user_bet, payline, won_money, spin_time) 
      VALUES('$uid', '$user_bet', '$payline', '$won_money', NOW())
    ");
    if ( !$res ) {
      $error_message = "Spin saving failed. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    return true;
  }

  public function save_options_in_db(){
    $db = DBconfig::get_instance();
    $this->playing =  $db->mysqli_link->real_escape_string( $this->playing );
    $this->paying_out =  $db->mysqli_link->real_escape_string( $this->paying_out );
    $this->maxbet =  $db->mysqli_link->real_escape_string( $this->maxbet );
    $this->minbet =  $db->mysqli_link->real_escape_string( $this->minbet );
    $this->percent_pay_to_referrers =  $db->mysqli_link->real_escape_string( $this->percent_pay_to_referrers );

    $res = $db->query("
      UPDATE slot_options SET
      `option_value` = '$this->playing'
      WHERE `option_name` = 'playing'
    ");
    if ( !$res ) {
      $error_message = "Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    $res = $db->query("
      UPDATE slot_options SET
      `option_value` = '$this->paying_out'
      WHERE `option_name` = 'paying_out'
    ");
    if ( !$res ) {
      $error_message = "Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    $res = $db->query("
      UPDATE slot_options SET
      `option_value` = '$this->maxbet'
      WHERE `option_name` = 'maxbet'
    ");
    if ( !$res ) {
      $error_message = "Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    $res = $db->query("
      UPDATE slot_options SET
      `option_value` = '$this->minbet'
      WHERE `option_name` = 'minbet'
    ");
    if ( !$res ) {
      $error_message = "Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    $res = $db->query("
      UPDATE slot_options SET
      `option_value` = '$this->percent_pay_to_referrers'
      WHERE `option_name` = 'percent_pay_to_referrers'
    ");
    if ( !$res ) {
      $error_message = "Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    return true;
  }

  public function get_option_from_db(){
    $db = DBconfig::get_instance();
    $options['playing'] = $db->mysqli_fetch_array("SELECT option_value FROM slot_options WHERE option_name = 'playing' ");
    $options['paying_out'] = $db->mysqli_fetch_array("SELECT option_value FROM slot_options WHERE option_name = 'paying_out' ");
    $options['maxbet'] = $db->mysqli_fetch_array("SELECT option_value FROM slot_options WHERE option_name = 'maxbet' ");
    $options['minbet'] = $db->mysqli_fetch_array("SELECT option_value FROM slot_options WHERE option_name = 'minbet' ");
    $options['percent_pay_to_referrers'] = $db->mysqli_fetch_array("SELECT option_value FROM slot_options WHERE option_name = 'percent_pay_to_referrers' ");

    $this->playing = $options['playing']['option_value'];
    $this->paying_out = $options['paying_out']['option_value'];
    $this->maxbet = $options['maxbet']['option_value'];
    $this->minbet = $options['minbet']['option_value'];
    $this->percent_pay_to_referrers = $options['percent_pay_to_referrers']['option_value'];
    return $options;
  }

  public function get_option($option_name){
    if (
        $option_name != 'playing' &&
        $option_name != 'paying_out' &&
        $option_name != 'maxbet' &&
        $option_name != 'minbet' &&
        $option_name != 'percent_pay_to_referrers' ){
      return false;
    }
    $this->get_option_from_db();
    switch ($option_name) {
      case 'paying_out':
        return $this->paying_out;
        break;
      case 'playing':
        return $this->playing;
        break;
      case 'maxbet':
        return $this->maxbet;
        break;
      case 'minbet':
        return $this->minbet;
        break;
      case 'percent_pay_to_referrers':
        return $this->percent_pay_to_referrers;
        break;
      default:
        return false;
        break;
    }
  }
          
  public function set_options( $received_options ){
    if ( $_SESSION['admin'] != true ){
      return false;
    }
    $setted_options = array();
    //check all received option names
    foreach( $received_options as  $option_name => $option_value){
      if ($option_name == 'paying_out'){
        $this->paying_out = ( $option_value == 'on' ) ? 'on' : 'off';
        $setted_options[$option_name] = $option_value;
      }
      elseif ( $option_name == 'playing' ) {
        $this->playing = ( $option_value == 'on' ) ? 'on' : 'off';
        $setted_options[$option_name] = $option_value;
      }
      elseif ( $option_name == 'maxbet' ) {
        $this->maxbet = ( is_numeric($option_value) ) ? $option_value : 0;
        $setted_options[$option_name] = $option_value;
      }
      elseif ($option_name == 'minbet') {
        $this->minbet = ( is_numeric($option_value) ) ? $option_value : 0;
        $setted_options[$option_name] = $option_value;
      }
      elseif ($option_name == 'percent_pay_to_referrers'){
        $this->percent_pay_to_referrers = ( is_numeric($option_value) ) ? $option_value : 0;
        $setted_options[$option_name] = $option_value;
      }
    }
    //and save all options in db
    if ($this->save_options_in_db()){
      return $setted_options;
    }
    else{
      $error_message = "Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return false;
    }
  }

  //make spin
  public function spin($bet_from_client, $client_seed){
    $user = $_SESSION['User'];
    //user hasn't logged in inaccessible to cash out money
    if ( $user->is_registered_and_not_logged() ){
      return array('new_payline' => -401, 'server_seeds' => -401);
    }
    //make all checks
    $this->get_option_from_db();
    if ($this->playing === 'off'){
      //Slot-machine powered off
      return -1;
    }
    if ( !$valid_bet_return = $this->is_valid_bet($bet_from_client) ){
      //echo '[Bet <= 0 or Bet not number.]';
      return -2;
    }
    if (!$this->is_valid_client_seed($client_seed)){
      return -3;
    }
    //save balance before spin
    $user_balance_before_spin = $this->user->money_balance;
    //place bet
    $this->currentBet = $bet_from_client;
    //bet was 
    $this->lastBet = $this->currentBet;
    $this->user->money_balance -= $this->currentBet;
    $this->currentBet = 0;
    //calculate win
    $arr_of_new_payline_and_servers_seeds = $this->get_new_payline_and_servers_seeds($client_seed);
    $new_payline = $arr_of_new_payline_and_servers_seeds['new_payline'];
    $paytable = Paytable::get_instance();
    $win_combination_name = $paytable->paylines_matching_with_wins($new_payline);
    //user gets money he won
    $won_money = $paytable->payoff_value($new_payline) * $bet_from_client;
    $this->user->money_balance += $won_money;
    //save balance after spin
    $user_balance_after_spin = $this->user->money_balance;
    $new_payline->bet_from_client = $bet_from_client;
    $new_payline->multiplier = $paytable->payoff_value($new_payline);
    //keep last payline//not used(?)
    $this->last_payline = $new_payline;
    $s = $this->user->save_in_db();
    $this->user->update_from_db();
    //logging every spin (by default)
    if ( $this->log_every_spin )
      $this->save_spin_in_db($this->user->uid, $bet_from_client, $win_combination_name, $won_money );
    $arr_of_new_payline_and_servers_seeds['new_payline']->bet_from_client = $new_payline->bet_from_client;
    $arr_of_new_payline_and_servers_seeds['new_payline']->multiplier = $new_payline->multiplier;

    //---affiliate part---
    //when balance was positive and becomes 0
    if ( $user_balance_before_spin > 0 && 0 == $user_balance_after_spin ){
      //it's time to calculate affiliate
      //                               user A                          user B
      //$this->affiliate_calculate( $this->user->affiliateusername, $this->user->uid );
      Slot::affiliate_calculate( $this->user->affiliateusername, $this->user->uid, $this->percent_pay_to_referrers );
    }
    return $arr_of_new_payline_and_servers_seeds;
  }

  /**
   * calculate bonus from affiliate program
   */
  public function affiliate_calculate( $affiliateusername, $referred_uid, $percent_pay_to_referrers ){// user A, user B
    if ( $affiliateusername == "" || $referred_uid == "" ){
      return false;
    }
    //all profit from user (B)
    $our_profit_from_user = round( Transaction::get_our_profit_from_user( $referred_uid ), 8 );//rounded to 0.12345678
    //get 30% from referred user (B) lost
    $total_earned = $our_profit_from_user * $percent_pay_to_referrers;
    //send to user (A) his cash back for his referral
    $affiliate_link = new AffiliateLink(  );
    $affiliate_link->get_affiliate_link( $affiliateusername, $referred_uid );
    $affiliate_link->total_earned = $total_earned;
    //add the cashback_bonus to user A
    if ( $affiliate_link->total_earned > $affiliate_link->payed ){
      $cashback_bonus = round( $affiliate_link->total_earned - $affiliate_link->payed, 8 );
      AffiliateLink::send_to_affiliate_cashback_bonus( $affiliateusername, $cashback_bonus);
      $affiliate_link->payed = $affiliate_link->total_earned;
    }
    //for not saving empty affiliate_links
    if ( $affiliate_link->is_valid() ){
      $affiliate_link->save_affiliate_link();
    }
  }

  /**
   * return array [ new randomly generated payline; and array of 3 server seeds ]
   * get new random number for all 3 reel..
   */
  public function get_new_payline_and_servers_seeds($client_seed){
    //hash client_seed, because it can be string of any symbols
    $hashed_client_seed = sha1($client_seed);
    //generate new server seeds
    $server_seeds = $this->getServerSeeds();
    for ($i = 0; $i < 3; $i++){
      //hash all 3 server seeds
      //..using client and server seed
      $syms[$i] = $this->reels[$i]->get_new_randomly_choosed_symbol($hashed_client_seed, $server_seeds[$i]);
    }
    $new_payline = new Payline($syms[0], $syms[1], $syms[2]);
    //generate new seeds HERE, because last seeds was used for getting payline above in this func
    $this->generateServerSeeds();
    $res_arr = array('new_payline' => $new_payline, 'server_seeds' => $server_seeds);
    //dump_it($res_arr);
    return $res_arr;
  }
  
  /**
   * get random generated 3 server seeds
   */
  public function generateServerSeeds(){
    for ($i = 0; $i < 3; $i++){
      $server_seeds[$i] = sha1(mt_rand());
    }
    $this->server_seeds = $server_seeds;
    return $server_seeds;
  }

  /**
   * just get 3 current server seeds
   */
  public function getServerSeeds(){
    if (!empty($this->server_seeds)){
      return $this->server_seeds;
    }
    else{
      $this->server_seeds = $this->generateServerSeeds();
    }
    return $this->server_seeds;
  }

  public function getHashedServerSeeds(){
    $server_seeds = $this->getServerSeeds();
    $json_server_seeds = json_encode($server_seeds);
    //wanna string? get it!
    return sha1($json_server_seeds);
  }
}

class WeightTable{
  //make it singletone
  protected static $weight_table;
  private function __construct(){}
  public function __destruct(){}
  private function __clone(){}
  public static function get_instance(){
    if (is_null(self::$weight_table)){
      self::$weight_table = new WeightTable();
      self::$weight_table->total_weight_table_filling();
      return self::$weight_table;
    }
    return self::$weight_table;
  }
  
  public $symbol_weight_reels;
  public function total_weight_table_filling(){
    //filling the weight for 1 reel
    $this->symbol_weight_reels[0][Symbol::$pyramid] = 4;
    $this->symbol_weight_reels[0][Symbol::$bitcoin] = 5;
    $this->symbol_weight_reels[0][Symbol::$anonymous] = 6;
    $this->symbol_weight_reels[0][Symbol::$onion] = 6;
    $this->symbol_weight_reels[0][Symbol::$anarchy] = 7;
    $this->symbol_weight_reels[0][Symbol::$peace] = 8;
    $this->symbol_weight_reels[0][Symbol::$blank] = 28;

    //filling the weight for 2 reel
    $this->symbol_weight_reels[1][Symbol::$pyramid] = 3;
    $this->symbol_weight_reels[1][Symbol::$bitcoin] = 4;
    $this->symbol_weight_reels[1][Symbol::$anonymous] = 4;
    $this->symbol_weight_reels[1][Symbol::$onion] = 5;
    $this->symbol_weight_reels[1][Symbol::$anarchy] = 5;
    $this->symbol_weight_reels[1][Symbol::$peace] = 6;
    $this->symbol_weight_reels[1][Symbol::$blank] = 37;

    //filling the weight for 3 reel
    $this->symbol_weight_reels[2][Symbol::$pyramid] = 1;
    $this->symbol_weight_reels[2][Symbol::$bitcoin] = 2;
    $this->symbol_weight_reels[2][Symbol::$anonymous] = 3;
    $this->symbol_weight_reels[2][Symbol::$onion] = 4;
    $this->symbol_weight_reels[2][Symbol::$anarchy] = 6;
    $this->symbol_weight_reels[2][Symbol::$peace] = 6;
    $this->symbol_weight_reels[2][Symbol::$blank] = 42;
    //total weight: 64 for every reel
  }

  /**
   * return the filled line (array) that consists of 64 symbols with considering the weight table
   */
  function get_symbols_reel_line($reel){
    //weight_arr == reel line
    $weight_arr = array(64);
    $current_num_in_weight_arr = 0;//counter in weight_arr
    //for every symbol
    foreach ($reel as $key => $value) {
      for ($i = 0; $i < $reel[$key]; $i++){
        $weight_arr[$current_num_in_weight_arr] = $key;
        //total number of processed cells
        $current_num_in_weight_arr++;
      }
    }
    return $weight_arr;
  }
}