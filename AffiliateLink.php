<?php

class AffiliateLink {
  public
    $affiliateusername,
    $referred_uid,
    $total_earned,
    $payed;
  public function __construct( $affiliateusername ='', $referred_uid ='', $total_earned = 0, $payed = 0 ){
    $this->affiliateusername = $affiliateusername;
    $this->referred_uid = $referred_uid;
    $this->total_earned = $total_earned;
    $this->payed = $payed;
  }
  public function __destruct() {}

  private function real_escape(){
    $db = DBconfig::get_instance();
    //escaping party
    $this->affiliateusername = $db->mysqli_link->real_escape_string( $this->affiliateusername );
    $this->referred_uid = $db->mysqli_link->real_escape_string( $this->referred_uid );
    $this->total_earned = $db->mysqli_link->real_escape_string( $this->total_earned );
    $this->payed = $db->mysqli_link->real_escape_string( $this->payed );
  }

  public function is_link_exist(){
    return AffiliateLink::is_link_exist_for( $this->affiliateusername, $this->referred_uid );
  }

  public static function is_link_exist_for( $affiliateusername, $referred_uid ){
    $db = DBconfig::get_instance();
    $link = $db->mysqli_fetch_array('
      SELECT * FROM affiliate_link
        WHERE `affiliateusername` = \''.$affiliateusername.'\'
        AND `referred_uid` = \''.$referred_uid.'\'
    ');
    //if there is no user with given uid
    if ( $link == FALSE ){
      $error_message = "[ The affiliate link doesn't exists. Probably a new one is created ] There is no user with given uid. affiliateusername = $affiliateusername and referred_uid = $referred_uid . File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    //the user is found
    else{
      return TRUE;
    }
  }

  public static function is_valid_for( $affiliateusername, $referred_uid, $total_earned, $payed ){
    if (
      $affiliateusername === '' ||
      $referred_uid === '' ||
      $total_earned === '' ||
      $payed === ''
    ){
      return false;
    }
    $affiliateusername_pattern = '/^[a-zA-Z0-9]{3,64}$/';
    if (
      !User::match_to_regexp( $affiliateusername_pattern, $affiliateusername ) ||
      !User::is_valid_uid( $referred_uid ) ||
      !is_numeric( $total_earned ) ||
      !is_numeric( $payed )
    ){
      return false;
    }
    //regexp shouldn't pass 'empty_r' because of '_' character, but just for case
    if ( $affiliateusername == 'empty_r' || $affiliateusername == 'not_pregmatch_or_not_exists' ){
      return false;
    }
    return true;
  }

  /**
   * @return bool if AffiliateLink has possible values
   */
  public function is_valid( ){
    return AffiliateLink::is_valid_for(
        $this->affiliateusername,
        $this->referred_uid,
        $this->total_earned,
        $this->payed
      );
  }

  public function save_affiliate_link( ){
    $this->real_escape();
    $db = DBconfig::get_instance();
    if ( !$this->is_link_exist( $this->affiliateusername, $this->referred_uid ) ){
      $res = $db->query("
        INSERT INTO affiliate_link ( affiliateusername, referred_uid, total_earned, payed )
        VALUES ('$this->affiliateusername', '$this->referred_uid', '$this->total_earned', '$this->payed' )
      ");
    }
    //if affiliate_link exists already just update record
    else {
      $res = $db->query( "
        UPDATE affiliate_link SET
        `total_earned` = '$this->total_earned',
        `payed` = '$this->payed'
        WHERE `affiliateusername` = '$this->affiliateusername' AND `referred_uid` = '$this->referred_uid'
      ");
    }
    if ( !$res ){
      $error_message = "save_affiliate_link failed. File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    //update affiliate_link after saving
    $this->get_affiliate_link( $this->affiliateusername, $this->referred_uid );
    return true;
  }

  public function get_affiliate_link( $affiliateusername, $referred_uid ){
    try{
      $db = DBconfig::get_instance();
      $referred_uid = $db->mysqli_link->real_escape_string( $referred_uid );
      $affiliateusername = $db->mysqli_link->real_escape_string( $affiliateusername );
      $arr_affiliate_link = $db->mysqli_fetch_array('
      SELECT * FROM affiliate_link
        WHERE `affiliateusername` = \''.$affiliateusername.'\'
        AND `referred_uid` = \''.$referred_uid.'\'
      ');
      if ( $arr_affiliate_link == false ){
        $error_message = "get_affiliate_link failed. Looking of affiliateusername = $affiliateusername and referred_uid = $referred_uid. File ".__FILE__." Line " .__LINE__;
        error_log( $error_message, 0 );
        return false;
      }
      else{
        $this->affiliateusername = $arr_affiliate_link['affiliateusername'];
        $this->referred_uid = $arr_affiliate_link['referred_uid'];
        $this->total_earned = $arr_affiliate_link['total_earned'];
        $this->payed = $arr_affiliate_link['payed'];
        return true;
      }
    }
    catch ( Exception $e ){
      $error_message = "get_affiliate_link failed. Looking of affiliateusername = $affiliateusername and referred_uid = $referred_uid. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
  }

  /**
   * method sends to user_A his 30% bonus
   * return amount of cashback bonus for affiliating
   */
  public static function send_to_affiliate_cashback_bonus( $affiliateusername, $cashback_bonus ){
    try{
      $db = DBconfig::get_instance();
      $uid = $db->mysqli_link->real_escape_string( $affiliateusername );
      $cashback_bonus = $db->mysqli_link->real_escape_string( $cashback_bonus );
      //get the current user A's incoming payment, that can be hanging in DB
      $arr_user_A_hanging_incoming_payment = $db->mysqli_fetch_array('
        SELECT `bitcoin_money_balance` FROM users WHERE username = \''.$affiliateusername.'\'
      ');
      $user_A_hanging_incoming_payment = $arr_user_A_hanging_incoming_payment['bitcoin_money_balance'];
      $user_A_hanging_incoming_payment += $cashback_bonus;
      $res = $db->query( "
        UPDATE users SET
        `bitcoin_money_balance` = '$user_A_hanging_incoming_payment'
        WHERE `username` = '$affiliateusername'
      ");
      if ( !$res ){
        $error_message = "send_to_affiliate_cashback_bonus failed. File ".__FILE__." Line " .__LINE__;
        error_log( $error_message, 0 );
        return FALSE;
      }
    }
    catch ( Exception $e ){
      $error_message = "send_to_affiliate_cashback_bonus failed. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
    return $cashback_bonus;
  }
}