function weightTable(){
  var table = this;
  //get array (reel1/2/3), symName, N - amount of symbols need to add to this array
  function fillxN(reel, symName, N){
    for(var i=0; i<N; i++){
      reel.push(symName);
    }
  }
  this.reels = new Array(
    //reel1
    new Array(),
    //reel2
    new Array(),
    //reel3
    new Array()
  );
  function fillTable(){
    var reel1 = table.reels[0];
    var reel2 = table.reels[1];
    var reel3 = table.reels[2];
    
    fillxN(reel1, 'pyramid', 4);
    fillxN(reel1, 'bitcoin', 5);
    fillxN(reel1, 'anonymous', 6);
    fillxN(reel1, 'onion', 6);
    fillxN(reel1, 'anarchy', 7);
    fillxN(reel1, 'peace', 8);
    fillxN(reel1, 'blank', 28);

    fillxN(reel2, 'pyramid', 3);
    fillxN(reel2, 'bitcoin', 4);
    fillxN(reel2, 'anonymous', 4);
    fillxN(reel2, 'onion', 5);
    fillxN(reel2, 'anarchy', 5);
    fillxN(reel2, 'peace', 6);
    fillxN(reel2, 'blank', 37);

    fillxN(reel3, 'pyramid', 1);
    fillxN(reel3, 'bitcoin', 2);
    fillxN(reel3, 'anonymous', 3);
    fillxN(reel3, 'onion', 4);
    fillxN(reel3, 'anarchy', 6);
    fillxN(reel3, 'peace', 6);
    fillxN(reel3, 'blank', 42);
  }
  
  this.getSymNameOnReelByNumber = function(reelN, symN){
    return this.reels[reelN][symN];
  }
  
  fillTable();
}