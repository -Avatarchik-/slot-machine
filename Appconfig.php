<?php
/**
 * Description of appconfig
 *
 * @author vadim24816
 */
if ($_SERVER['HTTP_HOST'] == 'bitbandit.eu'){
  AppConfig::$domainname = '.bitbandit.eu';
}
elseif ($_SERVER['HTTP_HOST'] == '109.174.40.94'){
  AppConfig::$domainname = '109.174.40.94';
}
elseif ($_SERVER['HTTP_HOST'] == 'localhost'){
  AppConfig::$domainname = 'localhost';
}
elseif ($_SERVER['HTTP_HOST'] == '127.0.0.1') {
  AppConfig::$domainname = '127.0.0.1';
}

//must be first!
ini_set('session.gc_maxlifetime', AppConfig::now_plus_x_years());
ini_set('session.cookie_lifetime', AppConfig::now_plus_x_years());

/*
 * Save all errors to "error_log" file
 */
error_reporting(E_ALL);
ini_set('error_log', "error_log");
ini_set('log_errors', true);

//set session via https
session_set_cookie_params( 
    AppConfig::now_plus_x_years(), 
    '/', //path
    AppConfig::$domainname,//current domain
    //'',//any
    //'.bitbandit.eu', //cant work on local
    true, //secure
    false //http only
  ); 
/*
  * First require classes which user session
  * */
require_once 'User.php';
require_once 'Slot.php';
require_once 'Reel.php';
require_once 'bitcoin/bitcoin.inc';
require_once 'MyBitcoinClient.php';
require_once 'Payline.php';
require_once 'Paytable.php';
require_once 'Symbol.php';

/*
 * Start SESSION for reading and writing
 */


session_start();

//relocate browser to https
require_once 'relocateToSecureScheme.php';

require_once 'Dumpit.php';
require_once 'DBconfig.php';
require_once 'Randomizer.php';
require_once 'Payline.php';
require_once 'Paytable.php';
require_once 'Transaction.php';
require_once 'AffiliateLink.php';

/*
 * Write to SESSION most important objects
 * Keep objects in SESSION for don't create them every time when a request got
 */
//User is duplicated in $_SESSION['User'] and $_SESSION['Slot']['User']
$_SESSION['MyBitcoinClient'] = (empty($_SESSION['MyBitcoinClient']))? MyBitcoinClient::get_instance() : $_SESSION['MyBitcoinClient'];
$_SESSION['User'] = (empty($_SESSION['User']))? User::get_instance() : $_SESSION['User'];
$_SESSION['Slot'] = (empty($_SESSION['Slot']))? Slot::get_instance($_SESSION['User']) : $_SESSION['Slot'];

require_once 'functions.php';

/*
 * Close $_SESSION for write until it will be need
 *
*/

//Cookies should be enabled
class AppConfig{
  public static $domainname = 'bitbandit.eu';
  public static $min_confirmations_for_cash_out = '2';
  //public static $message_bitcoin_show_when_user_withdrawn_money = 'Thank you for playing';
  public static function now_plus_x_years($x = 10){
    return time()+60*60*24*366*$x;
  }
}